// index.js

console.log("Hello World!");

// Array Methods

// JS has built-in methods to manipulate and manage our arrays.

/*
	MUTATOR METHODS - updates/mutates our array

	.push() - adds an item at the END of the array

	.unshift() - adds an item at the START of the array

	.pop() - removes an item at the END of array and return the removed item

	.shift() - removes an item at the START of array and return the removed item
	
	.splice() - remove all items starting from a specified index

	.sort() - sort our elements in an alphanumeric order

	.reverse() - reverses the order of how array elemets could be sorted

*/

// PUSH & POP

let koponanNiEugene = ["Eugene"];

koponanNiEugene.push("Vincent");
console.log(koponanNiEugene);

console.log(koponanNiEugene.push("Dennis"));
console.log(koponanNiEugene);

let removedItem = koponanNiEugene.pop();
console.log(koponanNiEugene);
console.log(removedItem);


// UNSHIFT & SHIFT

let fruits = ["Mango", "Kiwi","Apple"];
fruits.unshift("Pineapple");
console.log(fruits);

let computerBrands = ["Apple","Acer","Asus","Dell"];
computerBrands.shift(); //result: ['Acer', 'Asus', 'Dell']
console.log(computerBrands);


// SPLICE

console.log(computerBrands);
computerBrands.splice(1);
console.log(computerBrands); 

/*
 .splice(starting_index,delete_count) 
	- deletes a specified number of items from a starting index
*/
console.log("====================================");
console.log(fruits);
fruits.splice(0,1); 
console.log("fruits.splice(0,1);")
console.log(fruits); 

/*
 .splice(starting_index,0,elements_to_be_added) 
 	- add items from a starting index without deleting
*/

console.log("====================================");
console.log(koponanNiEugene);
koponanNiEugene.splice(1,0,"Dennis","Alfred");
console.log("koponanNiEugene.splice(1,0,'Dennis','Alfred');");
console.log(koponanNiEugene);

/*
 .splice(starting_index,delete_count,elements_to_be_added)
 	- delete an indicated number of items from a starting index and add elements
*/

console.log("====================================");
console.log(fruits);
fruits.splice(0,2,"Lime","Cherry");
console.log("fruits.splice(0,2,'Lime','Cherry');");
console.log(fruits);


/*
  splice, when removing elements, will be able to return the removed elements.
*/

console.log("====================================");
let item = fruits.splice(0);
console.log(fruits);
console.log(item);


// SORT & REVERSE

/*
  .sort() method by itself is used mostly for sorting arrays of strings. It performs differently when sorting numbers. There is a separate algorithm for doing so.
*/

console.log("====================================");

let members = ["Ben","Alan","Alvin","Jino","Tine"];
console.log(members);
members.sort();
console.log(members);

let numbers = [50,100,12,10,1];
console.log(numbers);
numbers.sort();
console.log(numbers);

numbers.sort(function(a, b){return a-b});
console.log(numbers);

console.log("====================================");

console.log(members);
members.reverse();
console.log(members);


/*
	NON-MUTATOR METHODS - methods not able to modify/change the original array when used

	.indexOf() 	
		- return index number of the FIRST matching element in the array
		- useful in finding index of item from an array with unknown total length
		- or finding item index from array that is constantly being added into or manipulated
		- if no match it returns -1

	.lastIndexOf() - returns index number of the LAST matching element in the array

	.slice() - copy a slice/portion of an array and return a new array from it

	.toString() - returns an array as a string separated by commas

	.join() - returns an array as a string with a specified separator

*/

console.log("====================================");

let carBrands = ["Vios","Fortuner","Crosswind","City","Vios","Starex"];
console.log(carBrands);


// .indexOf

let firstIndexofVios = carBrands.indexOf("Vios");
console.log("firstIndexofVios: ");
console.log(firstIndexofVios);

let indexOfStarex = carBrands.indexOf("Starex");
console.log("indexOfStarex: ");
console.log(indexOfStarex);

let indexOfBeetle = carBrands.indexOf("Beetle");
console.log("indexOfBeetle: ");
console.log(indexOfBeetle);

// .lastIndexOf()

let lastIndexOfVios = carBrands.lastIndexOf("Vios");
console.log("lastIndexOfVios: ");
console.log(lastIndexOfVios);

let indexOfMio = carBrands.lastIndexOf("Mio");
console.log("indexOfMio: ");
console.log(indexOfMio);

// .slice()

/*
	.slice(starting_index) 
	- copy an array into a new array with items from starting index to the last item

	.slice(starting_index,ending_index)
	- copy an array into a new array with items from starting index to just before ending index
*/

let shoeBrand = ["Jordan","Nike","Adidas","Converse","Sketchers"];
console.log("Shoe Brands:");
console.log(shoeBrand);


let myOwnedShoes = shoeBrand.slice(1); 
console.log("My Owned Shoes: slice(1)");
console.log(myOwnedShoes);
console.log("No change from original array:");
console.log(shoeBrand);

let herOwnedShoes = shoeBrand.slice(2,4);
console.log("Her Owned Shoes: slice(2,4)");
console.log(herOwnedShoes);

let heroes = ["Captain America","Superman","Spiderman","Wonder Woman","Hulk","Hawkeye","Dr. Strange"];


let myFaveHeroes = heroes.slice(2,6);
console.log(heroes);
console.log("My favourite superheroes: slice(2,6)");
console.log(myFaveHeroes);

// .toString()

let superHeroes = heroes.toString();
console.log(superHeroes);
console.log("My favourite heroes are " + heroes);
console.log("My favourite heroes are " + superHeroes);

// .join()

let superHeroes_2 = heroes.join();
console.log(superHeroes_2);

let superHeroes_3 = heroes.join(" ");
console.log(superHeroes_3);

let superHeroes_4 = heroes.join(1);
console.log(superHeroes_4);


/*
	ITERATOR METHODS -iterates or loops over the items in an array

	.forEach() 
		- similar to a for loop wherein it is able to iterate over the items in an array
		- able to repeat an action FOR EACH item in the array
		- takes an argument (anonymous function and cannot be invoked outside .forEach)
		- the function inside is able to receive the current item being looped

	.map() 
		- similar to .foreach() that it will iterate over all items and run a function for each item. However, with a map, whatever is returned in the function will be added into a new array that we can save.

	.includes()
		- returns a boolean if the item is in the array or not.
		- returns true if it is
		- else, returns false

*/

// .forEach

	// sample 1

let counter = 0;

heroes.forEach(
		function(hero){
			counter++;
			console.log(counter);
			console.log(hero);
		}
	)

	// sample 2

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard); //shows the multi-dimensional array

chessBoard.forEach(function(row){
	console.log(row); // list down each row with 8 elements
	row.forEach(function(square){
		console.log(square); //lists down each element for the whole multidimensional array
	})
})

	// sample 3

let numArr = [5,12,30,46,40];

numArr.forEach(function(number){

	if (number%5 === 0){
		console.log(number + " is divisible by 5");
	} else {
		console.log(number + " is not divisible by 5");
	}

});

/*
compared to the previous example from for loop (in s21):

let numArr = [5,12,30,46,40];

for (let index = 0; index < numArr.length; index++){

	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5");
	} else {
		console.log(numArr[index] + " is not divisible by 5");
	}

}
*/



// .map()

	// sample 1

console.log("let members = ['Ben','Alan','Alvin','Jino','Tine'];");
let instructors = members.map(function(member){

		return member + " is an instructor";
	})

console.log(instructors);
console.log(members);

	// sample 2

let numArr2 = [1,2,3,4,5];

let squareMap = numArr2.map(function(number){

		return number * number;
	})

console.log(numArr2);
console.log(squareMap);


/*
	map() vs forEach()

	- map is able to return an array
	- forEach simply iterates and does not return anything

*/

let squareForEach = numArr.forEach(function(number){

		return number * number
	})

console.log(squareForEach);


// .includes()

let isAMember = members.includes("Tine");
console.log(isAMember);

let isAMember2 = members.includes("Tee Jae");
console.log(isAMember2);